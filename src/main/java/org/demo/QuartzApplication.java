package org.demo;

import org.demo.quartz.QuartzTaskManager;
import org.demo.quartz.mode.CronTimingModel;
import org.demo.quartz.mode.IntervalTimingMode;
import org.demo.quartz.mode.TriggerType;
import org.demo.quartz.task.TestQuartz;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;

@SpringBootApplication
public class QuartzApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(QuartzApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //构建CRON定时
        //CronTimingModel cronTimingModel = new CronTimingModel(TestQuartz.class, "测试名", "测试组", "测试描述", "*/1 * * * * ?");
        // 构建固定间隔定时
        IntervalTimingMode intervalTimingMode = new IntervalTimingMode(TestQuartz.class, "测试名", "测试组", "测试描述", TriggerType.INTERVAL_SECOND, 5L,null);
        HashMap<String, Object> param = new HashMap<>();
        param.put("paramKey","获取到参数了");
        intervalTimingMode.setParam(param);
        QuartzTaskManager.getInstance().addTask(intervalTimingMode);
    }
}
